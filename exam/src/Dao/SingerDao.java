package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DBUtils.DBUtils;
import pojo.Singer;

public class SingerDao {
Connection connection = null;
PreparedStatement stmtInsert;
PreparedStatement stmtUpdate;
PreparedStatement stmtDelete;
PreparedStatement stmtSelect;
PreparedStatement stmtSearch;
public SingerDao() throws Exception {
	super();
	this.connection = DBUtils.getConnection();
	this.stmtInsert = this.connection.prepareStatement("INSERT INTO singer VALUES(?,?,?,?,?,?)");;
	this.stmtUpdate = this.connection.prepareStatement("UPDATE singer SET rating=? WHERE email_id=?");;
	this.stmtDelete = this.connection.prepareStatement("DELETE FROM singer WHERE email_id=?");;
	this.stmtSelect = this.connection.prepareStatement("SELECT * FROM singer");
	this.stmtSearch = this.connection.prepareStatement("select * from singer where name = ?");
}



public Singer search(String name) throws Exception {
	String sql = "select * from singer where name = '"+name+"'";
	ResultSet rs = this.stmtSearch.executeQuery(sql);
	Singer singer1 = null;
	while(rs.next()) {
	
		 singer1 = new Singer(rs.getInt("age") ,  rs.getInt("rating"), rs.getString("name"), rs.getString("gender"), rs.getString("contact"), rs.getString("email_id"));
				
      }
	
	
	return singer1;
	
}



public int  insert(Singer singer) throws Exception {
	this.stmtInsert.setString(1, singer.getName());
	this.stmtInsert.setString(2, singer.getGender());
	this.stmtInsert.setInt(3, singer.getAge());
	this.stmtInsert.setString(4, singer.getEmail_id());
	this.stmtInsert.setString(5, singer.getContact());
	this.stmtInsert.setInt(6, singer.getRating());

	return stmtInsert.executeUpdate();
	
}

public int update(String email_id , int rating) throws Exception {
	this.stmtUpdate.setFloat(1, rating);
	this.stmtUpdate.setString(2, email_id);
	return this.stmtUpdate.executeUpdate();
	
}

public int delete(String deleteEmailId) throws Exception {
	this.stmtDelete.setString(1, deleteEmailId);
	return stmtDelete.executeUpdate();
}




public List<Singer> getSinger() throws Exception {
	List<Singer> singers = new ArrayList<Singer>();
	ResultSet rs = this.stmtSelect.executeQuery();
	while(rs.next()) {
		
		Singer singer = new Singer(rs.getInt("age") ,  rs.getInt("rating"), rs.getString("name"), rs.getString("gender"), rs.getString("contact"), rs.getString("email_id"));
		
		singers.add(singer);
	}
	return singers;
}






}
