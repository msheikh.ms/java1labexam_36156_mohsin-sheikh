package pojo;

public class Singer {
	private int age,rating;
	private String name , gender , contact , email_id ;
	
	public Singer(int age, int rating, String name, String gender, String contact, String email_id) {
		super();
		this.age = age;
		this.rating = rating;
		this.name = name;
		this.gender = gender;
		this.contact = contact;
		this.email_id = email_id;
	}

	public Singer() {
		super();
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	@Override
	public String toString() {
		return "Singer [age=" + age + ", rating=" + rating + ", name=" + name + ", gender=" + gender + ", contact="
				+ contact + ", email_id=" + email_id + "]";
	}
	
}
