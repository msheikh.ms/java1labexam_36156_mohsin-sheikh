package DBUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtils {

	static Properties p;
	static {
		try {
			FileInputStream istream = new FileInputStream("config.properties");
			p = new Properties();
			p.load(istream);
			Class.forName(p.getProperty("DRIVER"));
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	public static Connection getConnection() throws Exception {
		return DriverManager.getConnection(p.getProperty("URL"), p.getProperty("USER"), p.getProperty("PASSWORD"));
	}
	
}
