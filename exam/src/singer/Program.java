package singer;

import java.util.List;
import java.util.Scanner;

import Dao.SingerDao;
import pojo.Singer;

public class Program {

	public static Scanner sc = new Scanner(System.in);
	
	public static int menulist() {
		System.out.println("enter your choise");
		System.out.println("0.exit");
		System.out.println("1.insert singer");
		System.out.println("2.delete singer");
		System.out.println("3.update ratings ");
		System.out.println("4.select all");
		System.out.println("5.search singer by name");

		return sc.nextInt();
		
	}
	
	
	public static void main(String[] args) throws Exception {
		int choice;
		SingerDao dao = new SingerDao();
		Singer singer = new Singer();
		
		while((choice=Program.menulist())!=0) {
			switch(choice) {
			case 1:
			 Singer singer1 =  Program.acceptRecord( );
				int count = dao.insert(singer1);
				System.out.println("no of row affected "+count);
				break;	
				
			case 2:
				
				String deleteId =	Program.deleteRecord();
				
				dao.delete(deleteId);
				System.out.println("deleted successfully ");
				break;	
				
			case 3:
				Program.updatePrice(dao);
				break;	
					
			case 4:
				List<Singer> singers = dao.getSinger();
				for (Singer sing1 : singers ) {
					System.out.println(sing1.toString());
				}	
				break;
				
			case 5:
				System.out.println("enter name" );
				sc.nextLine();
				String name=sc.nextLine();
				Singer sing=dao.search(name);
				System.out.println("name = "+sing.getName()+"age = "+sing.getAge()+"email = "+sing.getEmail_id()+"contact"+sing.getContact()+"gender = "+sing.getGender());
				break;
			}
		}
		
		
		
	}


	private static void updatePrice(SingerDao dao) throws Exception {
		System.out.println("enter singers email id ");
		String emailid = sc.nextLine();
		System.out.println("enter book's new  price ");
		int rating = sc.nextInt();
		
	int updatecount = dao.update(emailid, rating);	
	System.out.println("no of row affected"+ updatecount);
	}


	private static String deleteRecord() {
		System.out.println("enter email id of singer to be deleted");
		String id = sc.nextLine();
		
		return id;
	}


	private static Singer acceptRecord() {
		
		
		System.out.println("enter book singer name  ");
		sc.nextLine();
		String singerName = sc.nextLine();
		
		System.out.println("enter singer gender");
		String singerGender = sc.nextLine();
		
		System.out.println("enter singer age");
		int singerage = sc.nextInt();
		
		System.out.println("enter book email_id  ");
		String email = sc.nextLine();
		sc.nextLine();
		
		System.out.println("enter singer contact");
		String scontact = sc.nextLine();

		System.out.println("enter singer rating out of 10");
		int srating = sc.nextInt();
		
		
		
		Singer singer = new Singer(singerage, srating, singerName, singerGender, scontact, email);
		
		return singer;		
	}
}
